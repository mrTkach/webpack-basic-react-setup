*****************************************************
1. Установка webpack
// открываю терминал в каталоге и набираю команды

  npm init -y
  npm i -D webpack webpack-cli webpack-dev-server

--------------------------------------------

// По умолчанию компиляция проходить в режиме production и я получу WARNING in configuration, когда в терминале запущу команду webpack
// Это потому, что не указан mode
// Поэтому в терминале набираю команду

  webpack --mode=development


*****************************************************
2. Запуск webpack через файл package.json

// Чтобы постоянно не набирать к cli длинные команды, их лучше вынести в свойство "scripts"  файла package.json

 "scripts": {
    "webpack": "webpack",
    "dev": "npm run webpack --  --mode development --watch",
    "prod": "npm run webpack -- --mode production"
  }

*****************************************************
3. Конфигурационный файл webpack.config.js

// создаю ручками файл в корне webpack.config.js и добавляю следующий код

  module.exports =  (env = {mode: 'development'})  => {
      const isProduction = env.mode === 'production';
      return {
        mode: env.mode,
          entry: "./src/index.js",
          output: {
              path:   __dirname + "/dist",
              filename:"bundle.js"
          }
      }
  }

------------------------------

// Изменяю скрипты запуска в package.json

"scripts": {
    "webpack": "webpack",
    "dev": "npm run webpack --  --env.mode development --watch",
    "prod": "npm run webpack -- --env.mode production"
  }


*****************************************************
4. Loaders

Loaders указывают webpack как модифицировать файлы перед их добавлением

---------------- общий вид  loader
module: {
  rules: [
    { ... loader 1 ...},
    { ... loader 2 ...},
      .....
    { ... loader n ...},
  ]
}

-----------------------------------------------------------

// Компиляция происходит в синтаксисе ES 6, а его желательно его иметь в синтаксисе ES 5.
// Для этого надо использовать транспайлер babel

*****************************************************
5. Устанавливаю babel-loader:

  npm i -D babel-loader @babel/core @babel/preset-env


// добаляю в файл webpack.config.js "правила (rules)" после output

  module: {
    rules: [
      {  test: /\.js$/,   exclude: /node_modules/,    use: ['babel-loader'] },
    ],
  },


// создаю ручками системный файл .babelrc (с точкой в начале) в корне проекта для настроек babel, а именно указываю какие пресеты использовать и вставляю след. код:

  {
    "presets": ["@babel/preset-env"]
  }

// Отображение системных файлов по умолчанию отключено (google подскажет как включить отображение)

// перезапускаю команду "npm run dev" и вижу что bundle.js уже имеет синтаксис ES 5


*****************************************************
6. Устанавливаю webpack_dev_server

// в package.json дополняю запись "dev" в скриптах, а точнее заменяю на следующий код:

"scripts": {
    ...
    "dev": "webpack-dev-server  --env.mode development --progress --hide-modules --open",
    ...
  }
---------------------------

// Добавляю путь в файл webpack.config.js, вставляя первой строчкой следующий код:

  const path = require('path');

// Добавляю в webpack.config.js след. код после "module":

  devServer: {
    contentBase: path.join(__dirname),
    compress: true,
    port: 9000
  }

// (Следи за запятыми и свободными портами, если используешь docker или OpenServer/MAMP)

// если ошибка " webpack-dev-server: command not found", то устанавливаю

  npm install webpack-dev-server -g
