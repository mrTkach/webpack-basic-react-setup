const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (env = { mode: "development" }) => {
  const isProduction = env.mode === "production";
  return {
    mode: env.mode,
    entry: path.join(__dirname, "src", "index.js"),
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "bundle.js",
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.join(__dirname, "src", "index.html"),
      }),
    ],
    module: {
      rules: [
        {
          test: /\.js$/,
          include: path.resolve(__dirname, "src"),
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env", "@babel/react"],
            cacheDirectory: false,
          },
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: "style-loader",
            },
            {
              loader: "css-loader",
            },
          ],
        },
        {
          test: /\.svg$/,
          use: [
            {
              loader: "svg-loader",
            },
          ],
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader",
            },
          ],
        },
      ],
    },
    devServer: {
      static: path.join(__dirname, "src"),
      compress: true,
      port: 9000,
    },
  };
};
